#!/usr/bin/make -f

DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk
include /usr/share/dpkg/architecture.mk

v = 2.5
tclversions = 8.6 8.7
lib_so = libBLT.$(v).so
tcl_only_lib_so = libBLTlite.$(v).so
dtmp = $(shell pwd)/debian/tmp

%:
	dh $@

define CreateTargets

clean$(1):
	rm -rf build.$(1)

configure$(1):
	mkdir build.$(1)
	find . -mindepth 1 -maxdepth 1 -not -name "build.*" -exec cp -rf \{\} build.$(1) \;
	cd build.$(1) && \
	dh_auto_configure -- \
			  --build=$(DEB_BUILD_GNU_TYPE) \
			  --host=$(DEB_HOST_GNU_TYPE) \
			  --with-tcl=/usr/lib/$(DEB_HOST_MULTIARCH)/tcl$(1) \
			  --with-tk=/usr/lib/$(DEB_HOST_MULTIARCH)/tk$(1)

build$(1):
	cd build.$(1) && \
	$(MAKE) lib_so=$(lib_so).$(1) \
	        tcl_only_lib_so=$(tcl_only_lib_so).$(1)

install$(1):
	cd build.$(1) && \
	$(MAKE) lib_so=$(lib_so).$(1) \
		tcl_only_lib_so=$(tcl_only_lib_so).$(1) \
		INSTALL_ROOT=$(dtmp).$(1) install
	#
	# Fix up the pkgIndex.tcl for multiarch
	sed -i -e's:/usr/lib:/usr/lib/$(DEB_HOST_MULTIARCH):' $(dtmp).$(1)/usr/lib/blt$(v)/pkgIndex.tcl
	#
	# Now fix up the man pages
	mv $(dtmp).$(1)/usr/share/man/mann/BLT.n \
	   $(dtmp).$(1)/usr/share/man/mann/intro.n
	(cd $(dtmp).$(1)/usr/share/man/mann ; for i in *.n ; do \
	   sed -e's/^\.TH \+\([^ ]*\) \+n/.TH blt::\1 3tcl/' $$$$i >../man3/blt::`basename $$$$i .n`.3tcl ; \
	done)
	rm -r $(dtmp).$(1)/usr/share/man/mann
	#
	# Remove some misplaced documentation
	(cd $(dtmp).$(1)/usr/lib/blt$(v) ; rm README PROBLEMS NEWS)
	#
	# Now we need to move the demos and examples over to /usr/share
	mkdir -p $(dtmp).$(1)/usr/share/blt$(v)
	mv $(dtmp).$(1)/usr/lib/blt$(v)/demos $(dtmp).$(1)/usr/share/blt$(v)
	chmod a-x $(dtmp).$(1)/usr/share/blt$(v)/demos/*.txt
	#
	mkdir -p $(dtmp).$(1)/usr/share/blt$(v)/examples
	cp examples/* $(dtmp).$(1)/usr/share/blt$(v)/examples
	#
	# Patch the demos and examples to use wish$(1)
	(cd $(dtmp).$(1)/usr/share/blt$(v) ; \
	   for i in demos/*.tcl examples/*.tcl ; do \
		sed -e '1c#!/usr/bin/wish$(1)' $$$$i >$$$$i.new ; \
		mv $$$$i.new $$$$i ; \
		chmod 755 $$$$i; \
	   done)
	#
	chmod 644 $(dtmp).$(1)/usr/share/blt$(v)/demos/images/*
	chmod 644 $(dtmp).$(1)/usr/share/blt$(v)/demos/scripts/*.tcl
	chmod 755 $(dtmp).$(1)/usr/share/blt$(v)/demos/scripts/page.tcl

dh_makeshlibs$(1):
	dh_makeshlibs -p tk$(1)-blt$(v) -V 'tk$(1)-blt$(v) (>= $(v).3)'

dh_install$(1):
	dh_install -p tk$(1)-blt$(v) --sourcedir=$(dtmp).$(1)
	dh_install -p tk$(1)-blt$(v)-dev --sourcedir=$(dtmp).$(1)

endef

$(foreach tclv,$(tclversions),$(eval $(call CreateTargets,$(tclv))))

override_dh_clean: $(foreach tclv,$(tclversions),clean$(tclv))
	dh_clean

override_dh_auto_configure: $(foreach tclv,$(tclversions),configure$(tclv))

override_dh_auto_build: $(foreach tclv,$(tclversions),build$(tclv))

override_dh_auto_install: $(foreach tclv,$(tclversions),install$(tclv))
	find debian

override_dh_install: $(foreach tclv,$(tclversions),dh_install$(tclv))
	dh_install -p blt --sourcedir=$(dtmp).$(word 1,$(tclversions))
	dh_install -p blt-dev --sourcedir=$(dtmp).$(word 1,$(tclversions))
	dh_install -p blt-demo --sourcedir=$(dtmp).$(word 1,$(tclversions))

override_dh_installdocs:
	dh_installdocs
	rm -f debian/blt-dev/usr/share/doc/blt-dev/html/*.vc

override_dh_makeshlibs: $(foreach tclv,$(tclversions),dh_makeshlibs$(tclv))

get-orig-source:
	CURDIR=`pwd` && TMPDIR=`mktemp -d /tmp/blt.XXXXXX` && \
	cd $$TMPDIR && \
	wget -O tmp.zip \
	     http://prdownloads.sourceforge.net/wize/blt-src-$(v).3.zip && \
	unzip tmp.zip && \
	rm -rv blt$(v)/library/dd_protocols && \
	(cd blt$(v) && patch -p1 <$$CURDIR/debian/patches/source.patch) && \
	sed -i -e 's/\$$(ddFiles)//' blt$(v)/library/Makefile.in && \
	tar -Jcf $$CURDIR/blt_$(v).3+dfsg.orig.tar.xz blt$(v) && \
	rm -rf $$TMPDIR

.PHONY: override_dh_clean override_dh_auto_configure override_dh_auto_build \
	override_dh_auto_install override_dh_makeshlibs override_dh_installdocs \
	get-orig-source
